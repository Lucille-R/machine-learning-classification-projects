# -*- coding: utf-8 -*-
import csv, random, numpy as np 

# open "got.csv" file
datafile=open("got.csv","r")
file=csv.reader(datafile,delimiter=";")

# convert the open csv file in list
db=list(file)
sizedb=len(db)-1
#print(db) #check


## Function to define a dictionary from a list
## databaseToConvert & listAttributsNotToConvert must be a list
## nameOfDic must be a dictionary
## variables are changed by default in float 
## this function needs to be improve to integrate a more fit type conversion procedure 
def f_createDic(databaseToConvert,nameOfDic, listAttributesNotToConvert=None, convertType=float):
    
    if type(databaseToConvert) != list:
        print("The first argument of f_createDic function need to be changed in list")
    
    if type(nameOfDic)!=dict:
        print("The second argument of f_createDic function need to be changed in dictionary")
    
    keys=databaseToConvert[0] #list to store variables' name
    del databaseToConvert[0] #remove the first row
    
    condition=[1]*len(listAttributesNotToConvert)
    lastAttributes=len(listAttributesNotToConvert)-1
    conditionIf="("
    if listAttributesNotToConvert != None:
        
        for i in range(len(listAttributesNotToConvert)):
            
            condition[i]="i!="+str(listAttributesNotToConvert[i])
            #print(condition[i]) #check
            
            if i==lastAttributes:
                conditionIf=str(conditionIf)+condition[i]+")"
            else :
                conditionIf=str(conditionIf)+condition[i]+" and " 
            
        #print(conditionIf) #check
            
                
    # Dictionary filling :
    
    nb_row = [1] * len(databaseToConvert)
    for i in range(len(keys)):
        name_key=keys[i]
        for j in range(len(nb_row)):
            if listAttributesNotToConvert==None:
                databaseToConvert[j][i]=convertType(databaseToConvert[j][i])
                #print(databaseToConvert[j][i])
            else :
                if eval(conditionIf):
                    databaseToConvert[j][i]=convertType(databaseToConvert[j][i])
                    #print(databaseToConvert[j][i])    
            nameOfDic[name_key,j]=databaseToConvert[j][i]
    
    return(nameOfDic)
    
numColumnAttributesNotToConvert=[1,3,4]    
got={}
f_createDic(db,got,numColumnAttributesNotToConvert)


#check 
'''print(got["name",2])   
print(got['isAlive',7])
print(db[7][14])
print(type(got["book1",1]))
print(type(got["culture",1]))'''


#Function to compute a distance between two examples
def f_distance(example1,example2, criteria, dbDictionary,poids=1):
    
    if ((type(example1) and type(example2)) != int):
        print("The first & the second arguments of this function need to be integers")
        
    if type(criteria) != list:
        print("The third argument of this function need to be changed in list")
    
    if type(dbDictionary) != dict:
        print("The fourth argument of this function need to be changed in dictionary")
        
    dist=0
    
    for i in range(len(criteria)):
        #print(criteria[i]) #check
        if (type(dbDictionary[criteria[i],example1])==float and type(dbDictionary[criteria[i],example2])==float):
            absSubstraction=abs(dbDictionary[criteria[i],example1]-dbDictionary[criteria[i], example2])
            dist=dist+absSubstraction
        else :
            if dbDictionary[criteria[i],example1]!=dbDictionary[criteria[i],example2]:
                dist=dist+1
        #print("dist:"+str(dist)) #check

    return(dist)

#Check :
example1_Got=0 #Arya
example2_Got=2 #Bran
example3_Got=3 #Cersei
criterion_Got=["house","male", "culture","book1","book2", "book3", "book4","book5","numDeadRelations","isMarried","isNoble","popularity"]

Aria_Bran=f_distance(example1_Got,example2_Got,criterion_Got,got)
print("Distance between "+str(got["name",example1_Got])+" and"+str(got["name",example2_Got])+" :"+str(Aria_Bran))

Aria_Cersei=f_distance(example1_Got, example3_Got, criterion_Got,got)
print("Distance between "+str(got["name",example1_Got])+" and"+str(got["name",example3_Got])+" :"+str(Aria_Cersei))


#Function to extract the nearest K neighbours of an example, with estimation of prediction quality on "isAlive" attribute
def f_K_NN(exampleToPred, attributeToPred, dbDictionary_KNN, sizedbDictionary, K, criteria_KNN, sizeDbToLearn=None, functionDist=f_distance,poids=1):
    
    resultPred={} #store exits of this function in a dictionary
    
    if dbDictionary_KNN[attributeToPred,exampleToPred]==0:
        predValue=0
        #print("We have to predict that "+str(dbDictionary_KNN["name",exampleToPred])+" is dead.")
    elif dbDictionary_KNN[attributeToPred,exampleToPred]==1:
        predValue=1
        #print("We have to predict that "+str(dbDictionary_KNN["name",exampleToPred])+" is alive.")
    
    listLearning=range(0,sizedbDictionary)
    
    # Let you choose how many examples from the database you use to search for KNN
    # None is the default version of this function in which you search for KNN on every examples in the database
    if sizeDbToLearn==None:
        p_sizeToSearchKNN=sizedbDictionary
    else:
        p_sizeToSearchKNN=sizeDbToLearn
        
    list=[]#List to store informations about KNN
    distExample=[1]*p_sizeToSearchKNN
    u=0
     
    while u < p_sizeToSearchKNN:
        distExample[u]=functionDist(exampleToPred,listLearning[u],criteria_KNN,dbDictionary_KNN,poids)
        tuple=(distExample[u],u,dbDictionary_KNN["name",u])
        list.append(tuple) 
        
        u+=1 
        
    list.sort() #sorting the list in ascending order on distance criteria 
        
    K=K+1
    #verify that you have enough examples defined in the argument "sizeDbToLearn" of the function 
    if (sizeDbToLearn!=None and K>sizeDbToLearn): 
        print("Not enough examples in your learning database")
    else: 
        k_neighbours=list[1:K] #to exclude the same example than exampleToPred
    
    k_id=[1]*len(k_neighbours) #list to store the id of the neighbour extracted from tuple
    name=[1]*len(k_neighbours) #list to store the name of the neighbour extracted from tuple
    distanceK=[1]*(len(k_neighbours)) #list to store the distance between the neighbour and the example
    
    for i in range(len(k_neighbours)):
        n=k_neighbours[i]
        k_id[i]=n[1] #extract from tuple, the id of the neighbour
        name[i]=n[2] #extract from tuple the name of the neighbour
        distanceK[i]=n[0] #extract from tuple the distance between the example and the neighbour
        resultPred["distance_KNN",k_id[i]]=distanceK[i]
    
    resultPred["id_KNN"]=k_id
    resultPred["name_KNN"]=name
    
    sum_alive=0
    for l in range(len(k_id)):
        alive=dbDictionary_KNN[attributeToPred,k_id[l]]
        sum_alive=sum_alive+alive
    #print(sum_alive)
      
    mean_alive=sum_alive/len(k_id)
    #print(mean_alive)
    
    if mean_alive > 0.5:
        prediction="the character is alive with a probability of : "+str(mean_alive)
        if predValue==0:
            predQuality="bad prediction"
        else:
            predQuality="good prediction"
    elif mean_alive==0.5:
        prediction="Wanted "+str(dbDictionary_KNN['name',exampleToPred])+" ! Please return Dead and Alive to Erwin Schrodinger"
        predQuality="bad prediction"
    else:
        prediction="the character is alive with a probability of : "+str(mean_alive)
        if predValue==1:
            predQuality="bad prediction"
        else:
            predQuality="good prediction"
    
    resultPred["prediction_KNN"]=prediction
    resultPred["predQuality_KNN"]=predQuality
            
    return(resultPred)
    # resultPred is built with five keys : 
    #"distance_KNN",int : for each k neighbours' id, let you retrieve distance 
    #"id_KNN" : list of the k-neighbours' id 
    #"name_KNN": list of the k-neigbours' character name
    #"prediction_KNN" : string about the probability of being alive
    #"predQuality_KNN" : string about the quality of this prediction

resDaennerys=f_K_NN(4, "isAlive", got, sizedb, 10, criterion_Got)
#check
'''print(resDaennerys["id_KNN"])
print(resDaennerys["name_KNN"])
print(resDaennerys["prediction_KNN"])
print(resDaennerys["predQuality_KNN"])'''
      
resArya=f_K_NN(0, "isAlive", got, sizedb, 10, criterion_Got,1000)#def with not the default size of the database

#check 
'''print(resArya["id_KNN"])
print(resArya["name_KNN"])
print(resArya["prediction_KNN"])
print(resArya["predQuality_KNN"])'''


#Function to test many examples of the database on f_KNN() and compute a global prediction success rate on "isAlive" attribute
def f_compute_SuccessRate(attributeToPred, dbDictionary_SR, sizedbDictionary, K_neighbours,criteria_KNN,sizeDbToTest=None, sizeDbToLearn=None, functionDist=f_distance,poids=1):
    
    Success=0
    
    #randomization of examples : 
    id_example =range(0,sizedbDictionary)
    random.shuffle(id_example)
    #print(id_example) #check
    
    if sizeDbToTest == None:
        p_sizeTest = sizedbDictionary #all the database is used to compute the successRate
    else :
        p_sizeTest = sizeDbToTest #let you decide on how many examples you want to compute the successRate
        
    
    i_ex=0
    while i_ex < p_sizeTest :
        findKNN=f_K_NN(id_example[i_ex], attributeToPred, dbDictionary_SR, sizedbDictionary, K_neighbours, criteria_KNN, sizeDbToLearn,functionDist,poids)
       
        qualitePred=findKNN["predQuality_KNN"]
    
        if qualitePred == "good prediction":
            Success=Success+1
            #print(Success) #check
            
        i_ex+=1

    SucessRate=(Success/float(p_sizeTest))*100
    
    return(SucessRate)

#sizedb=len(db)-1 #reminder

#Check :
'''tx=f_compute_SuccessRate("isAlive", got,sizedb,10,criterion_Got)
print("Le taux de bonne prédiction est de "+str(tx))

tx_1000Examples=f_compute_SuccessRate("isAlive", got,sizedb,10,criterion_Got, 1000)
print("Le taux de bonne prédiction est de "+str(tx_1000Examples))'''


#Test the f_distance function for different values of K 
## Remove ''' to check : 
'''fileName="K_SucessRate"
Test_F_Improved=open(fileName+".csv", "wb")
writeFile=csv.writer(Test_F_Improved,delimiter=";")
writeFile.writerow(["Prediction Success Rate","Number of K Neighbours"])

kNeighbours=2
while kNeighbours < 51:
  print(kNeighbours)  
  Rate=f_compute_SuccessRate("isAlive", got,sizedb,kNeighbours,criterion_Got)
  print(Rate)
  writeFile.writerow([Rate,kNeighbours])
  
  kNeighbours+=1 '''
  

#Improved function to compute a distance between two examples
def f_distanceImproved(example1,example2, criteria, dbDictionary,poids):
    
    if ((type(example1) and type(example2)) != int):
        print("The first & the second arguments of this function need to be integers")
        
    if type(criteria) != list:
        print("The third argument of this function need to be changed in list")
    
    if type(dbDictionary) != dict:
        print("The fourth argument of this function need to be changed in dictionary")
        
    dist=0
    
    for i in range(len(criteria)):
        
        if (type(dbDictionary[criteria[i],example1])==float and type(dbDictionary[criteria[i],example2])==float):
            if (criteria[i]=="popularity" or criteria[i]=="nameDeadRelations"):
                absSubstraction=abs(dbDictionary[criteria[i],example1]-dbDictionary[criteria[i], example2])
                absSubstraction=absSubstraction*poids
            else : 
                absSubstraction=abs(dbDictionary[criteria[i],example1]-dbDictionary[criteria[i], example2])

            dist=dist+absSubstraction
            
        else :
            if dbDictionary[criteria[i],example1]!=dbDictionary[criteria[i],example2]:
                dist=dist+1
        #print("dist:"+str(dist)) #check

    return(dist)  

resDaennerysImproved=f_K_NN(4, "isAlive", got, sizedb, 10, criterion_Got,functionDist=f_distanceImproved)
#check
'''print(resDaennerysImproved["id_KNN"])
print(resDaennerysImproved["name_KNN"])
print(resDaennerysImproved["prediction_KNN"])
print(resDaennerysImproved["predQuality_KNN"])'''
      
resAryaImproved=f_K_NN(0, "isAlive", got, sizedb, 10, criterion_Got,1000,functionDist=f_distanceImproved)#def with not the default size of the database

#check 
'''print(resAryaImproved["id_KNN"])
print(resAryaImproved["name_KNN"])
print(resAryaImproved["prediction_KNN"])
print(resAryaImproved["predQuality_KNN"])'''

tx_f_Improved=f_compute_SuccessRate("isAlive", got,sizedb,10,criterion_Got, functionDist=f_distanceImproved)
print(tx_f_Improved)


#Test the f_distanceImproved function for different values of Weight and K
## Remove ''' to check : 
'''fileName2="Test_FunctionImproved"
Test_F_Improved=open(fileName2+".csv", "wb")
writeFile2=csv.writer(Test_F_Improved,delimiter=";")
writeFile2.writerow(["Number of K Neighbours","Prediction Success Rate","Poids"])


kNeighbours=20
while kNeighbours < 31:
  print(kNeighbours) 
  p=1
  while p < 10:
      Rate=f_compute_SuccessRate("isAlive", got,sizedb,kNeighbours,criterion_Got,functionDist=f_distanceImproved,poids=p)
      print(Rate)
      
      writeFile2.writerow([kNeighbours, Rate, p])
      
      p+=1
  
  kNeighbours+=1 '''

